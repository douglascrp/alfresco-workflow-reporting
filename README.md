Workflow Reporting
==================

This project is a dashlet for the Alfresco Share that carry out an overview on workflow status and processes. 
It improves monitoring tasks and making best decisions.


Watch the video
---------------

https://www.youtube.com/watch?v=ClDk47Ilmbg&feature=youtu.be

License
-------
Copyright 2016 Spectrum groupe.

Licensed under the Apache License, Version 2.0; you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Features
--------

+ Autocomplete the chosen workflow definition.
+ Filter the workflow instances by date range.
+ Control of the dates and the workflow definitions.
+ Generate 2 reports (Line or stacked column).
+ Display workflow instances by 3 status:
     - In progress
     - Completed
     - Overdue
+ Export reports in png format.
+ Pop up all instances by selected date in graphics.
+ Post up selected workflow details from this previous popup.


Installation
------------

The component has been developed to install on top of an existing Alfresco 4.2, 5.0, 5.1 or 5.2 installation. 
The 'workflow-reporting-repo-<version>.amp' or 'workflow-reporting-share-<version>.amp' needs to be installed into the Alfresco 
Repository / Share webapp using the Alfresco Module Management Tool:

    java -jar alfresco-mmt.jar install workflow-reporting-repo-<version>.amp /path/to/alfresco.war
    java -jar alfresco-mmt.jar install workflow-reporting-share-<version>.amp /path/to/share.war
	
See http://docs.alfresco.com/5.1/tasks/amp-install.html for more details.

	
ChartJS
-------

To generate reports, Workflow Reporting uses a javascript open source library: Chart.js.
for more details visit:
		http://www.chartjs.org/